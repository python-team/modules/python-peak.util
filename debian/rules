#!/usr/bin/make -f

PKG = python-peak.util
VERSION = $(shell dpkg-parsechangelog -S Version | sed 's,-.*,,')

UPSTREAMS = \
	addons-0.7 \
	bytecodeassembler-0.6 \
	extremes-1.1.1 \
	proxies-0.9 \
	symboltype-1.0

# This is needed by setup.py multiplexer.
export SUBDIRS = $(subst .,-,$(UPSTREAMS))

PKG_PREFIX = peak.util-

USCAN_EXTRA_ARGS = --repack --download --destdir $(GOS_TMP_DIR)/
TAR_EXTRA_ARGS = --owner root --group root --mode a+rX

%:
	dh $@ --with python2

override_dh_auto_install:
	dh_auto_install -- --single-version-externally-managed
	rm -f debian/$(PKG)/usr/lib/python*/dist-packages/*.pth

get-orig-source:
	$(eval GOS_TMP_DIR := $(shell mktemp -d $(PKG)-XXXXXXXXX))
	set -e; \
	for upstream in $(UPSTREAMS); do \
		uscan --no-conf \
			--watchfile debian/$(PKG_PREFIX)`echo $$upstream | cut -d- -f1`.watch $(USCAN_EXTRA_ARGS) \
			--upstream-version 0 --download-version `echo $$upstream | cut -d- -f2` \
			--package `echo $(PKG_PREFIX)$$upstream | cut -d- -f1-2`; \
		mv $(GOS_TMP_DIR)/`echo $(PKG_PREFIX)$$upstream | cut -d- -f1-2`_`echo $$upstream | cut -d- -f2`.orig.tar.gz \
		   $(GOS_TMP_DIR)/$(PKG)_$(VERSION).orig-`echo $$upstream | tr . -`.tar.gz; \
	done
	# Create manually the empty main package
	mkdir $(GOS_TMP_DIR)/$(PKG)-$(VERSION)
	cd $(GOS_TMP_DIR); GZIP=--best; tar -cz $(TAR_EXTRA_ARGS) -f $(PKG)_$(VERSION).orig.tar.gz $(PKG)-$(VERSION)
	rmdir $(GOS_TMP_DIR)/$(PKG)-$(VERSION)
	mv $(GOS_TMP_DIR)/$(PKG)_$(VERSION).orig* $(CURDIR)
	rm -Rf $(GOS_TMP_DIR)
