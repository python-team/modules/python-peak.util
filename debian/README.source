This repository contains only the `debian` directory because git-buildpackage
doesn't handle multiple upstream tarballs, see #561071 for details.

Upgrade to a new upstream
=========================

This package use multiple upstream tarballs support provided by source package
format "3.0 (quilt)".

To obtain new orig tarballs you have to specify package version inside
debian/changelog, update module version in the $(UPSTREAMS) Makefile variable
inside debian/rules and then invoke the get-orig-source target of debian/rules
as follows:

    $ debian/rules get-orig-source

If all goes well, that should leave, inside the the current directory, a
tarball called

    PACKAGE_YYYYMMDD.orig.tar.gz

    where PACKAGE is the source package name and YYYYMMDD is the version date
    in year/month/day format specified in debian/changelog;

and a series of packages called

    PACKAGE_YYYYMMDD.orig-MODULENAME-MODULEVERSION.tar.gz

    where MODULENAME and MODULEVERSION are specified in the $(UPSTREAMS)
    Makefile variable inside debian/rules.


Adding a new utility
====================

As this is a utility collection package, it is natural to expect that
in the future new utilities will be added. To add one to the package
follow a few simple rules:

1) add the module name and version (usually the egg name) to the $(UPSTREAMS)
   Makefile variable in debian/rules

2) add the per-utility watch file as debian/NAME.watch, where NAME is
   the module name

3) add license and copyright information about the new module to
   debian/copyright

4) run "debian/rules get-orig-source" to recreate the new tarball

5) if the new module has docs, have a look at debian/*.{install,dirs}
   to ship it where appropriate

 -- Stefano Zacchiroli <zack@debian.org>, Wed, 10 Jun 2009 16:45:07 +0200
 -- Daniele Tricoli <eriol@mornie.org>, Thu, 04 Feb 2016 23:29:23 +0100
